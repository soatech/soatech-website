#!/bin/bash

#########
# Setup #
#########

# 1. Run s3cmd with the --configure flag
# s3cmd --configure
# 2. Copy AWS credentials as prompted.  Use HTTPS and encryptions as desired.
# 3. Update command to point at desired bucket.
# 4. Add files as neccessary to the --exclude flag

# gets the S3 path.  Keep out of version control if you want to keep your bucket secret
source deploy.properties

if [ -z "$S3_PATH_SOATECH_COM" ]; then
	echo "Setup your S3_PATH_SOATECH_COM variable first"
	exit
fi

promptyn () {
    while true; do
        read -p "$1 " yn
        case $yn in
            [Yy]* ) return 0;;
            [Nn]* ) return 1;;
            * ) echo "Please answer yes or no.";;
        esac
    done
}

echo "Deploying to:"
echo $S3_PATH_SOATECH_COM
echo ""

if promptyn "Continue? (y/n)"; then
	s3cmd sync --exclude-from='s3.exclude' --delete-removed --no-mime-magic -M * $S3_PATH_SOATECH_COM
fi