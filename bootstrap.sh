#!/usr/bin/env bash

SETUP_DEPLOY=true
SETUP_BASH=false

DEV_USER="vagrant"
CODE_FOLDER="/vagrant"

apt-get update

# generic software
# apt-get install -y wget
apt-get install -y vim wget build-essential python2.7 nano

if $SETUP_DEPLOY; then
    apt-get install -y curl python-setuptools

    cd ~/
    wget http://sourceforge.net/projects/s3tools/files/s3cmd/1.5.2/s3cmd-1.5.2.tar.gz/download -O s3cmd-1.5.2.tar.gz
    tar -zxvf s3cmd-1.5.2.tar.gz
    cd s3cmd-1.5.2
    python setup.py install
fi

# setup fancy pants vim
# http://www.intelesyscorp.com/blog/awesome-vim-configuration
if $SETUP_BASH; then
    cd ~/
    wget https://github.com/drmikehenry/vimfiles/archive/master.tar.gz
    tar -zxvf master.tar.gz
    mv vimfiles-master .vim
    cd .vim
    python setup.py

    # better bashrc and help file for root
    source $CODE_FOLDER/env-templates/user-config.sh

    # configure the vagrant user
    su -c "source $CODE_FOLDER/env-templates/user-config.sh" $DEV_USER
fi
