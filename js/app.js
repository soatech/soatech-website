/**
 * Created by eric on 3/25/2015.
 */
var Main = (function () {
    function Main() {
        //-------------------------------------------------------------------------
        //
        // Event Handlers
        //
        //-------------------------------------------------------------------------
        this.carousel_slidHandler = function (event) {
            $(event.relatedTarget).find('.imagery').animate({ opacity: 1.0 }, 2000);
        };
        this.carousel_slideHandler = function (event) {
            $(event.relatedTarget).find('.imagery').css({ opacity: 0 });
            $(event.currentTarget).find('.imagery').animate({ opacity: 0.0 });
        };
    }
    //-------------------------------------------------------------------------
    //
    // Methods
    //
    //-------------------------------------------------------------------------
    Main.prototype.init = function () {
        this.configuration();
        this.addListeners();
        this.showActiveImages();
    };
    Main.prototype.addListeners = function () {
        var _this = this;
        $('.carousel').on('slide.bs.carousel', (function (e) { return _this.carousel_slideHandler(e); }));
        $('.carousel').on('slid.bs.carousel', (function (e) { return _this.carousel_slidHandler(e); }));
    };
    Main.prototype.configuration = function () {
        jQuery.fn.visible = function () {
            return this.css('visibility', 'visible');
        };
        jQuery.fn.invisible = function () {
            return this.css('visibility', 'hidden');
        };
        jQuery.fn.visibilityToggle = function () {
            return this.css('visibility', function (i, visibility) {
                return (visibility == 'visible') ? 'hidden' : 'visible';
            });
        };
    };
    Main.prototype.showActiveImages = function () {
        $('.carousel .item.active').find('.imagery').animate({ opacity: 1.0 }, 2000);
    };
    return Main;
})();
var main = new Main();
main.init();
//# sourceMappingURL=app.js.map