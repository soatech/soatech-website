/**
 * Created by eric on 3/25/2015.
 */

class Main {

    //-------------------------------------------------------------------------
    //
    // Methods
    //
    //-------------------------------------------------------------------------

    init() {
        this.configuration();

        this.addListeners();

        this.showActiveImages();
    }

    addListeners() {
        $('.carousel').on('slide.bs.carousel', (e => this.carousel_slideHandler(e)));
        $('.carousel').on('slid.bs.carousel', (e => this.carousel_slidHandler(e)));
    }

    configuration() {
        jQuery.fn.visible = function() {
            return this.css('visibility', 'visible');
        };

        jQuery.fn.invisible = function() {
            return this.css('visibility', 'hidden');
        };

        jQuery.fn.visibilityToggle = function() {
            return this.css('visibility', function(i, visibility) {
                return (visibility == 'visible') ? 'hidden' : 'visible';
            });
        };
    }

    showActiveImages() {
        $('.carousel .item.active').find('.imagery').animate({opacity: 1.0}, 2000);
    }

    //-------------------------------------------------------------------------
    //
    // Event Handlers
    //
    //-------------------------------------------------------------------------

    carousel_slidHandler = (event) => {
        $(event.relatedTarget).find('.imagery').animate({opacity: 1.0}, 2000);
    };

    carousel_slideHandler = (event) => {
        $(event.relatedTarget).find('.imagery').css({opacity: 0});
        $(event.currentTarget).find('.imagery').animate({opacity: 0.0});
    };
}

var main:Main = new Main();
main.init();